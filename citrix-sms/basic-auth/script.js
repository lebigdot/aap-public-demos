/**
 * Replacement for the sample Citrix customisation file
 * 
 * Author  : Adam Polka
 * Version : see the VERSION constant near the top of the module code
 * 
 * Modifies the login experience for users using SMS for 2FA:
 * - hides the first login failure when they request the SMS code via the Passcode field
 * - pre-populates the Username and Password fields on the second login attempt, so that they only have to type in the SMS code
 * 
 * NB: this is not expected to work in Internet Explorer
 */


 
/**
 * Using strict mode 
 */
"use strict";



/******************************************
 * Using the module pattern for jQuery code  
 ******************************************/
$( document ).ready( function() {

	
	
	/***************************************
	 * Module aapModule start
	 * 
	 * @return  {function}  Public functions
	 ***************************************/
    const aapModule = ( function() {		



        /*************************************
         * FLAGS TO CONTROL SCRIPT'S BEHAVIOUR
         *************************************/

         /** @type {boolean} */
         const FLAG_FILL_FIELDS = true;

        /********************************************
         * END OF FLAGS TO CONTROL SCRIPT'S BEHAVIOUR
        ********************************************/     


        /*******************
         * SCRIPT VERSION
         *******************/

         /** @type {string} */
         const VERSION = "basic-auth.4";

        /*******************/

        /** @type {string} */
        const CHAR_HASH = "#";

        /** @type {string} */
        const ERROR_MSG_TRIGGER = "Incorrect user name or password.";

        /** @type {string} */
        const ID_ERROR_ELEMENT = "access_denied_title";

        /** @type {string} */
        const ID_FIELD_USERNAME = "login";

        /** @type {string} */
        const ID_FIELD_PASSWORD = "passwd";

        /** @type {string} */
        const ID_FIELD_MFA = "passwd1";

        /** @type {string} */
        const ID_MSG_REPLACEMENT = "login_title";

        /** @type {string} */
        const MFA_SMS = "sms";

        /** @type {string} */
        const SMS_INFO_ID = "itv-sms-info";

        /** @type {string} */
        const SMS_INFO_MSG = "Please enter the code received by SMS. If you do not receive an SMS, the username or password may be wrong. Check / re-enter and request an SMS again.";

        /** @type {string} */
        const SMS_INFO_CLASS = "confirmation";

        /** @type {string} */
        const SELECTOR_FORM_PARENT = "div.form-container";

        /** @type {string} */
        var enteredUsername = "";

        /** @type {string} */
        var enteredPassword = "";
        
        /** @type {boolean} */
        var isLastMfaSms = false;


        /**
		 * Executed when document ready - intial setup
		 *
		 * @return  {void}		
		 */ 
		const onReady = function() {

            console.log( "AAP customisation script loaded - initialising. Version: " + VERSION );

            // Attach listeners
            $( SELECTOR_FORM_PARENT ).on( "input", handlerInputFields );

            // Set up MutationObservers
            setUpFormMutationObserver();
        };   



        /**
		 * MutationObserver to watch and intercept the form changes
		 *
		 * @return  {void}		
		 */
        const setUpFormMutationObserver = function() {
            
            /** @type {string} */
            var errorMsg = "";

            // Select the node that will be observed for mutations
            const targetNode = document.querySelector( SELECTOR_FORM_PARENT );

            // Options for the observer (which mutations to observe)
            const config = { childList: true, subtree: true };

            // Callback function to execute when mutations are observed
            const callback = function( mutationsList, observer ) {            
                errorMsg = $( CHAR_HASH + ID_ERROR_ELEMENT ).text();
                
                if ( isLastMfaSms && errorMsg == ERROR_MSG_TRIGGER ) {
                    IntervenePostSmsRequest();
                } else {
                    console.log( "Other mutation" );
                }
            };

            // Create an observer instance linked to the callback function
            const observer = new MutationObserver( callback );

            // Start observing the target node for configured mutations
            observer.observe( targetNode, config );
        };  



        /**
         * Make the required modifications post SMS request to improve user experience
         *
         * @return  {void} 
         */
        const IntervenePostSmsRequest = function() {
            
            console.log( "Intervening to improve SMS experience - hiding the error message, adding info message" );
            $( CHAR_HASH + ID_ERROR_ELEMENT ).css('visibility', 'hidden');

            if ( $( CHAR_HASH + ID_MSG_REPLACEMENT ).text() != SMS_INFO_MSG ) {
                $( CHAR_HASH + ID_MSG_REPLACEMENT ).text( SMS_INFO_MSG ).addClass( SMS_INFO_CLASS ).css( "margin-left", "30px" );
            }

            if ( FLAG_FILL_FIELDS ) {
                console.log( "Intervening to improve SMS experience - flag set, prefilling the password" );
                $( CHAR_HASH + ID_FIELD_USERNAME ).val( enteredUsername );
                $( CHAR_HASH + ID_FIELD_PASSWORD ).val( enteredPassword );
                $( CHAR_HASH + ID_FIELD_MFA ).focus();
            } else {
                $( CHAR_HASH + ID_FIELD_USERNAME ).focus();
            }
        };



        /**
         * Event handler for form input fields, reacts to MFA field changes
         *
         * @param   {Object}    event   Event object
         * @return  {void} 
         */
        const handlerInputFields = function( event ) {

            if ( event.target.id == ID_FIELD_MFA ) {
                ProcessMfaField();
            } else if ( isLastMfaSms && [ ID_FIELD_USERNAME, ID_FIELD_PASSWORD ].includes( event.target.id ) ) {
                CaptureUserData();
            }
        };



        /**
         * Checks the value of MFA field and reacts as appropriate
         *
         * @return  {void} 
         */
        const ProcessMfaField = function() {

            /** @type {string} */
            var mfaValue = $( CHAR_HASH + ID_FIELD_MFA ).val().toString().toLowerCase();
            
            if ( mfaValue == MFA_SMS ) {
                ActOnMfaSms();
            } else {
                ActOnMfaOther();
            }
        };



        /**
         * Act on MFA = SMS
         *
         * @return  {void} 
         */
        const ActOnMfaSms = function() {

            console.log( "SMS request detected" );
            isLastMfaSms = true;
            CaptureUserData();            
        };



        /**
         * Capture user login info
         *
         * @return  {void} 
         */
        const CaptureUserData = function() {

            // only capture if will be used
            if ( FLAG_FILL_FIELDS ) {
                enteredUsername = $( CHAR_HASH + ID_FIELD_USERNAME ).val().toString();
                enteredPassword = $( CHAR_HASH + ID_FIELD_PASSWORD ).val().toString();
                console.log( "Saved data for user " + enteredUsername + ", password length " + enteredPassword.length );
            }            
        };

        

        /**
         * Act on MFA = other
         *
         * @return  {void} 
         */
        const ActOnMfaOther = function() {

            console.log( "Not SMS request" );

            // do not need these any more, clear everything
            ClearSavedUserInfo();
            isLastMfaSms = false;
        };       
        
        

        /**
         * Clear user info saved from form
         *
         * @return  {void} 
         */
        const ClearSavedUserInfo = function() {

            console.log( "Clearing any saved user data" );
            enteredUsername = "";
            enteredPassword = "";
        };



        /*************************************************************************
    	 * Code below will be executed when the function is read - keep at the end
    	 *************************************************************************/	
        
        // Return public functions and variables
        return {
            onReady: onReady
        };


        
    })();
	
	/*********************
	 * Module aapModule end
	 *********************/	
 
	
	
	/*************************************************
	 * Actual $( document ).ready() action starts here
	 *************************************************/		
    aapModule.onReady();
	
});
