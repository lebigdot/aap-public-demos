/**
 * Replacement for the sample Citrix customisation file
 * 
 * Author  : Adam Polka
 * Version : see the VERSION constant near the top of the module code
 * 
 * Modifies the login experience for users using SMS for 2FA:
 * - hides the first login failure when they request the SMS code via the Passcode field
 * - pre-populates the Password field on the second login attempt, so that they only have to type in the SMS code
 * 
 */


 
/**
 * Using strict mode 
 */
"use strict";



/******************************************
 * Using the module pattern for jQuery code  
 ******************************************/
$( document ).ready( function() {

	
	
	/***************************************
	 * Module aapModule start
	 * 
	 * @return  {function}  Public functions
	 ***************************************/
    const aapModule = ( function() {		



        /*************************************
         * FLAGS TO CONTROL SCRIPT'S BEHAVIOUR
         *************************************/

         /** @type {boolean} */
         const FLAG_FILL_PASSWORDS = true;

        /********************************************
         * END OF FLAGS TO CONTROL SCRIPT'S BEHAVIOUR
        ********************************************/     


        /*******************
         * SCRIPT VERSION
         *******************/

         /** @type {string} */
         const VERSION = "nfactor.3";

        /*******************/

        /** @type {string} */
        const CHAR_HASH = "#";

        /** @type {string} */
        const ERROR_MSG_TRIGGER = "Try again or contact your help desk";

        /** @type {string} */
        const ID_FIELD_USERNAME = "login";

        /** @type {string} */
        const ID_FIELD_PASSWORD = "passwd";

        /** @type {string} */
        const ID_FIELD_MFA = "passwd1";

        /** @type {string} */
        const MFA_SMS = "sms";

        /** @type {string} */
        const SMS_INFO_ID = "itv-sms-info";

        /** @type {string} */
        const SMS_INFO_MSG = "Please enter the code received by SMS. <br><br> If you do not receive the SMS, you may have entered the wrong password - try deleting it and typing it again.";

        /** @type {string} */
        const SMS_INFO_DIV = '<div class="field CredentialTypenone"><div class="left"><div id="' + SMS_INFO_ID + '" class="standaloneText label confirmation" role="alert"></div></div></div>';

        /** @type {string} */
        const SELECTOR_FORM_PARENT = "div.form-container";

        /** @type {string} */
        const SELECTOR_FOR_INFO_MSG = "form.form.insertPoint.credentialform div.field.CredentialTypesavecredentials";

        /** @type {string} */
        var enteredUsername = "";

        /** @type {string} */
        var enteredPassword = "";
        
        /** @type {boolean} */
        var isLastMfaSms = false;


        /**
		 * Executed when document ready - intial setup
		 *
		 * @return  {void}		
		 */ 
		const onReady = function() {

            console.log( "AAP customisation script loaded - initialising. Version number: " + VERSION );

            // Attach listeners
            $( SELECTOR_FORM_PARENT ).on( "input", handlerInputFields );

            // Set up MutationObservers
            setUpFormMutationObserver();
        };   



        /**
		 * MutationObserver to watch and intercept the form changes
		 *
		 * @return  {void}		
		 */
        const setUpFormMutationObserver = function() {
            
            /** @type {string} */
            var errorMsg = "";

            // Select the node that will be observed for mutations
            const targetNode = document.querySelector( SELECTOR_FORM_PARENT );

            // Options for the observer (which mutations to observe)
            const config = { childList: true, subtree: true };

            // Callback function to execute when mutations are observed
            const callback = function( mutationsList, observer ) {            
                errorMsg = $( "div.field.CredentialTypenone span.detail-text.error" ).text();
                
                if ( isLastMfaSms && errorMsg == ERROR_MSG_TRIGGER ) {
                    IntervenePostSmsRequest();
                } else {
                    console.log( "Other mutation" );
                }
            };

            // Create an observer instance linked to the callback function
            const observer = new MutationObserver( callback );

            // Start observing the target node for configured mutations
            observer.observe( targetNode, config );
        };  



        /**
         * Make the required modifications post SMS request to improve user experience
         *
         * @return  {void} 
         */
        const IntervenePostSmsRequest = function() {
            
            console.log( "Intervening to improve SMS experience - hiding the error message, adding info message" );
            $( "div.field.CredentialTypenone span.detail-text.error" ).css('visibility', 'hidden');

            if ( $( CHAR_HASH + SMS_INFO_ID ).length == 0 ) {
                $( SELECTOR_FOR_INFO_MSG ).before( SMS_INFO_DIV );
                $( CHAR_HASH + SMS_INFO_ID ).html( SMS_INFO_MSG );                
            }

            if ( FLAG_FILL_PASSWORDS ) {
                console.log( "Intervening to improve SMS experience - flag set, prefilling the password" );
                $( CHAR_HASH + ID_FIELD_PASSWORD ).val( enteredPassword );
                $( CHAR_HASH + ID_FIELD_MFA ).focus();
            } else {
                $( CHAR_HASH + ID_FIELD_PASSWORD ).focus();
            }
        };



        /**
         * Event handler for form input fields, reacts to MFA field changes
         *
         * @param   {Object}    event   Event object
         * @return  {void} 
         */
        const handlerInputFields = function( event ) {

            if ( event.target.id == ID_FIELD_MFA ) {
                ProcessMfaField();
            } else if ( event.target.id == ID_FIELD_PASSWORD && isLastMfaSms ) {
                CaptureUserData();
            }
        };



        /**
         * Checks the value of MFA field and reacts as appropriate
         *
         * @return  {void} 
         */
        const ProcessMfaField = function() {

            /** @type {string} */
            var mfaValue = $( CHAR_HASH + ID_FIELD_MFA ).val().toString().toLowerCase();
            
            if ( mfaValue == MFA_SMS ) {
                ActOnMfaSms();
            } else {
                ActOnMfaOther();
            }
        };



        /**
         * Act on MFA = SMS
         *
         * @return  {void} 
         */
        const ActOnMfaSms = function() {

            console.log( "SMS request detected" );
            isLastMfaSms = true;
            CaptureUserData();            
        };



        /**
         * Capture user login info
         *
         * @return  {void} 
         */
        const CaptureUserData = function() {

            // only capture if will be used
            if ( FLAG_FILL_PASSWORDS ) {
                enteredUsername = $( CHAR_HASH + ID_FIELD_USERNAME ).val().toString();
                enteredPassword = $( CHAR_HASH + ID_FIELD_PASSWORD ).val().toString();
                console.log( "Saved data for user " + enteredUsername + ", password length " + enteredPassword.length );
            }            
        };

        

        /**
         * Act on MFA = other
         *
         * @return  {void} 
         */
        const ActOnMfaOther = function() {

            console.log( "Not SMS request" );

            // do not need these any more, clear everything
            ClearSavedUserInfo();
            isLastMfaSms = false;
        };       
        
        

        /**
         * Clear user info saved from form
         *
         * @return  {void} 
         */
        const ClearSavedUserInfo = function() {

            console.log( "Clearing any saved user data" );
            enteredUsername = "";
            enteredPassword = "";
        };



        /*************************************************************************
    	 * Code below will be executed when the function is read - keep at the end
    	 *************************************************************************/	
        
        // Return public functions and variables
        return {
            onReady: onReady
        };


        
    })();
	
	/*********************
	 * Module aapModule end
	 *********************/	
 
	
	
	/*************************************************
	 * Actual $( document ).ready() action starts here
	 *************************************************/		
    aapModule.onReady();
	
});
